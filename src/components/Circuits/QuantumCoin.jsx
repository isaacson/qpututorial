import React from 'react';
import {Bar} from 'react-chartjs-2';
import 'jsqubits';
//import slider from '../Slider/Slider';

const qubits = require('jsqubits').jsqubits;

function QuantumCoinFlip() {
  return qubits('|0>').hadamard(qubits.ALL).measure(qubits.ALL).result;
}

let heads = 0;
let tails = 0;

const initialState = {
  labels: ['|0>','|1>'],
  datasets: [
    {
      label: 'Quantum Coin',
      data: [heads,tails],
      backgroundColor: 'red',
    }
  ]
};

class QuantumCoin extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      plot: initialState
    };
  }

  addFlips(flips) {
    for(let i = 0; i < flips; i++) {
      let result = QuantumCoinFlip();
      if(result === 0) {
        heads++;
      } else {
        tails++;
      }
    }

    var newState = {
      ...initialState,
      datasets: [
        {
          label: 'QuantumCoin',
          data: [heads, tails],
          backgroundColor: 'red',
        }
      ]
    };
    this.setState({
      plot: newState
    });
  }

  render() {
    return(
      <div>
      <Bar data={this.state.plot}
      options={{
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }}
      />
      <input id="nFlips" type="range" min="1" max="10000" defaultValue="10" step="1"/>
      <button 
        onClick={() => {
          var nFlips = document.getElementById("nFlips").value;
          this.addFlips(nFlips)
        }}
      >
        Submit
      </button>
      </div>
    );
  }
}

export default QuantumCoin;
