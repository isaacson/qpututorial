import React from 'react';
import ReactDOM from 'react-dom';
//import {Bar} from 'react-chartjs-2';
//import 'jsqubits';
import './index.css';
import QuantumCoin from './components/Circuits/QuantumCoin';

//const qubits = require('jsqubits').jsqubits;

//function Hadamard() {
//  return qubits('|0>').hadamard(qubits.ALL).measure(qubits.ALL).result;
//}
//
//var heads = 0;
//var tails = 0;
//
//const initialState = {
//  labels: ['|0>','|1>'],
//  datasets: [
//    {
//      label: 'Quantum Coin',
//      data: [heads,tails]
//    }
//  ]
//};
//
//class Clock extends React.Component {
//  constructor(props) {
//    super(props);
//    this.state = {
//      date: new Date(),
//      plot: initialState, 
//      count: 0
//    };
//  }
//
//  componentDidMount() {
//    this.timerID = setInterval(
//      () => this.tick(),
//      1000
//    );
//  }
//
//  componentWillUnmount() {
//    clearInterval(this.timerID);
//  }
//
//  tick() {
//    this.setState({
//      date: new Date(),
//    });
//  }
//
//  addFlips(flips) {
//    for(let i = 0; i < flips; i++) {
//      var result = Hadamard();
//      if(result === 0) {
//        heads++;
//      } else {
//        tails++;
//      }
//    }
//    var newState = {
//      ...initialState,
//      datasets: [
//        {
//          label: 'Quantum Coin',
//          data: [heads,tails]
//        }
//      ]
//    };
//    this.setState({
//      date: new Date(),
//      plot: newState,
//    });
//  }
//
//  render() {
//    return(
//      <div>
//      <Bar data={this.state.plot}
//      options={{
//        scales: {
//          yAxes: [{
//            ticks: {
//              beginAtZero: true
//            }
//          }]
//        }
//      }}
//      />
//      <button 
//      onClick={() => this.addFlips(1)} >
//      Add One Flip
//      </button>
//      <button 
//      onClick={() => this.addFlips(10)} >
//      Add 10 Flips
//      </button>
//      <button 
//      onClick={() => this.addFlips(100)} >
//      Add 100 Flips
//      </button>
//      <button 
//      onClick={() => this.addFlips(1000)} >
//      Add 1000 Flips
//      </button>
//      </div>
//    );
//  }
//}

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div>
      <div>
      <QuantumCoin />
      </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

